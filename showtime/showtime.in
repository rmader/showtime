#!@PYTHON@

# showtime.in
#
# Copyright 2024 kramo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import os
import sys
import signal
from pathlib import Path
import locale
import gettext
from platform import system

pkgdatadir = "@pkgdatadir@"
localedir = "@localedir@"

sys.path.insert(1, pkgdatadir)
signal.signal(signal.SIGINT, signal.SIG_DFL)

if system() == "Linux":
    locale.bindtextdomain("showtime", localedir)
    locale.textdomain("showtime")
    gettext.install("showtime", localedir)
else:
    gettext.install("showtime")

if __name__ == "__main__":
    import gi

    from gi.repository import Gio, GLib

    try:
        # For a macOS application bundle
        resource = Gio.Resource.load(
            str(Path(__file__).parent / "Resources" / "showtime.gresource")
        )
    except GLib.GError:
        resource = Gio.Resource.load(os.path.join(pkgdatadir, "showtime.gresource"))
    resource._register()

    from showtime import main

    sys.exit(main.main())
